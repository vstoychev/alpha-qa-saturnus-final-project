# Test Report
The purpose of this document is to provide a summary of the results of tests performed as outlined within this document.
## Contents
1. Overview of Tests Results  
1.1. Tests log  
1.2. Overall assessment of tests  
2. Detailed Test Cases Report  
3. Detailed Bugs Report  
4. Test Cases covered by automated API tests  
5. Test Cases covered by automated UI tests
6. Test execution reports  
7. Conclusion  

## 1. Overview of Tests Results
### 1.1. Tests log
Give a few information about tests.
The WEare web-based application was tested on local Docker test environment, from the 2021/07/05 to the 2021/07/29. The tests of the Saturnus project test plan were executed by:  
*	Teodora Futekova
*	Vangel Stoychev

### 1.2. Overall assessment of tests
* All 73 drafted test cases were executed
* 38.6 % of the high priority test cases have failed
* 15 test cases were covered by automated API tests
* 7 test cases were covered by automated UI tests
* 50 bugs were logged

| Test Cases Statistics  |    |    |     |
|-----------------|----|----|-----|
| Priority/Status |Pass|Fail|Total|
| High            |27  |17  |44   |
| Medium          |10  |12  |22   |
| Low|6|1|7|
| Total (Issues) |43|30|73|

| Bugs Statistics  |    |    |     | |
|-----------------|----|----|-----|-|
Priority/Severity|Low|Medium|High|Total |
High|11|10|6|27
Medium|3|4|2|9
Low|11|3|0|14
Total (Issues)|25|17|8|50

## 2. Detailed Test Cases Report

Key|Summary|Assignee|Reporter|Priority|Status
|--|--|--|--|--|--|
AQS-1|Anonymous user search by registered user valid profession and first name|Teodora|Teodora|High|Fail
AQS-5|Anonymous user search by registered user valid profession|Teodora|Teodora|High|Pass
AQS-6|Anonymous user search by registered user valid first name|Teodora|Teodora|High|Pass
AQS-7|Anonymous user search by registered user valid last name|Teodora|Teodora|High|Fail
AQS-8|Anonymous user views public posts feed by category|Teodora|Teodora|High|Pass
AQS-9|Anonymous user views registered user post additional information (comments)|Teodora|Teodora|High|Fail
AQS-10|Anonymous user registers with valid credentials in all fields|Teodora|Teodora|High|Pass
AQS-11|Admin user edits other user profile|Teodora|Teodora|Low|Pass
AQS-12|Admin user disables other user profile|Teodora|Teodora|High|Fail
AQS-13|Admin user edits public post with picture|Teodora|Teodora|High|Pass
AQS-14|Admin user edits private post with picture|Teodora|Teodora|High|Fail
AQS-15|Admin user edits other user comment|Teodora|Teodora|High|Pass
AQS-16|Admin user deletes other user comment|Teodora|Teodora|High|Pass
AQS-17|Admin user attempts to disable own profile|Teodora|Teodora|Low|Fail
AQS-18|Anonymous user searches all users by leaving search fields empty|Teodora|Teodora|High|Pass
AQS-19|Anonymous user registers with valid credentials in required fields|Teodora|Teodora|High|Pass
AQS-20|Anonymous user search by part of valid profession in lower case|Teodora|Teodora|High|Pass
AQS-21|Anonymous user attempts to register with an empty form|Teodora|Teodora|High|Pass
AQS-22|Anonymous user attempts to view registered user profile|Teodora|Teodora|High|Pass
AQS-23|Anonymous user search by part of valid first name in lower case|Teodora|Teodora|High|Pass
AQS-24|Anonymous user attempts to register with invalid required fields|Teodora|Teodora|Medium|Fail
AQS-25|Anonymous user search nonexistent profession and valid first name|Teodora|Teodora|Low|Pass
AQS-26|Anonymous user search valid profession and nonexistent last name|Teodora|Teodora|Low|Pass
AQS-27|Registered user edits profile with valid personal information in all fields|Vangel|Vangel|High|Fail
AQS-28|Registered user edits profile with valid personal information in all required fields|Vangel|Vangel|High|Fail
AQS-29|Registered user changes professional profile industry|Vangel|Vangel|High|Pass
AQS-30|Registered user edits professional profile services and availability with valid values|Vangel|Vangel|High|Pass
AQS-31|Registered user edits profile by uploading picture|Vangel|Vangel|High|Pass
AQS-32|Registered user changes profile picture visability to private|Vangel|Vangel|High|Fail
AQS-33|Registered user changes profile picture visability to public|Vangel|Vangel|High|Fail
AQS-34|Registered user sends connect request|Vangel|Vangel|High|Pass
AQS-35|Registered user approves connect request|Vangel|Vangel|High|Pass
AQS-36|Registered user disconnects connection|Vangel|Vangel|High|Pass
AQS-37|Registered user creates private post without picture with short text|Vangel|Vangel|High|Pass
AQS-38|Registered user creates public post with picture and short text|Vangel|Vangel|High|Pass
AQS-39|Registered user edits the text and changes the picture with valid text and picture of post|Vangel|Vangel|High|Fail
AQS-40|Registered user deletes own post|Vangel|Vangel|High|Pass
AQS-41|Registered user creates comment with valid text|Vangel|Vangel|High|Fail
AQS-42|Registered user edits comment with valid text|Vangel|Vangel|High|Fail
AQS-43|Registered user deletes comment|Vangel|Vangel|High|Fail
AQS-44|Registered user attempts to login with empty credentials|Vangel|Vangel|High|Pass
AQS-45|Registered user attempts to edit profile with empty fields|Vangel|Vangel|High|Pass
AQS-46|Registered user attempts to create post with empty text field|Vangel|Vangel|High|Fail
AQS-47|Registered user attempts to create comment with empty text|Vangel|Vangel|High|Fail
AQS-48|Registered user attempts to login with wrong password|Vangel|Vangel|Medium|Pass
AQS-49|Registered user attempts to login with wrong username|Vangel|Vangel|Medium|Pass
AQS-50|Registered user views his latest activity in profile page|Vangel|Vangel|Medium|Fail
AQS-51|Registered user browses public posts after creating post|Vangel|Vangel|Medium|Fail
AQS-52|Registered user explores category after creating post|Vangel|Vangel|Medium|Fail
AQS-53|Registered user likes post|Vangel|Vangel|Medium|Pass
AQS-54|Registered user dislikes posts|Vangel|Vangel|Medium|Pass
AQS-55|Registered user creates post with text with maximum allowed symbols|Vangel|Vangel|Medium|Pass
AQS-56|Registered user dislikes comment|Vangel|Vangel|Medium|Pass
AQS-57|Registered user attempts to update profile with invalid data in fields|Vangel|Vangel|Medium|Pass
AQS-58|Registered user attempts to create post and select text file instead of picture|Vangel|Vangel|Medium|Fail
AQS-59|Registered user attempts to create post with text length bigger than allowed|Vangel|Vangel|Medium|Pass
AQS-60|Registered user attempts to create comment with text length bigger than allowed|Vangel|Vangel|Medium|Pass
AQS-61|Registered user creates comment with text with maximum allowed symbols|Vangel|Vangel|Medium|Fail
AQS-62|Registered user continues session after session expiration|Vangel|Vangel|Low|Pass
AQS-63|Registered user attempts to edit post of another user|Vangel|Vangel|Low|Pass
AQS-64|Registered user attempts to edit comment of another user|Vangel|Vangel|Low|Pass
AQS-70|Admin user deletes post without picture|Teodora|Teodora|High|Pass
AQS-71|Admin user deletes post with picture|Teodora|Teodora|High|Pass
AQS-72|Admin user views private posts|Teodora|Teodora|Medium|Fail
AQS-73|Admin user views private profile pictures|Teodora|Teodora|Medium|Fail
AQS-74|Run Link Checker|Teodora|Teodora|Medium|Fail
AQS-75|Run HTML/CSS Validation|Teodora|Teodora|Medium|Fail
AQS-76|Check navigation bar functionalities|Vangel|Teodora|High|Fail
AQS-77|Check for spelling, grammar and punctuation mistakes|Teodora|Teodora|Medium|Fail
AQS-78|Check DevTools console|Vangel|Teodora|Medium|Fail
AQS-79|Anonymous user registers with valid credentials including numbers/symbols in required fields|Teodora|Teodora|High|Fail
AQS-80|Registered user likes comment|Vangel|Vangel|Medium|Pass
AQS-81|Registered user logs in with valid credentials|Vangel|Vangel|High|Pass


## 3. Detailed Bugs Report

Key|Summary|Reporter|Priority|Resolution
--|--|--|--|--|
AQSI-1|Entering correct terms in both search fields doesn't produce existing results|Teodora|Medium|Unresolved
AQSI-2|"No continue search function when user gets the ""no users existing in this search criteria"" message"|Teodora|High|Unresolved
AQSI-3|User not found when searching by correct last name|Teodora|Medium|Unresolved
AQSI-4|Search field is free-form whereas the target is selected from a dropdown menu|Teodora|Medium|Unresolved
AQSI-5|No comments are shown along the post content|Teodora|Medium|Unresolved
AQSI-6|"Discrepancy between anchor element (button) name/text ""SIGN IN"" and target link page ""Login"""|Vangel|High|Unresolved
AQSI-7|User is not navigated to profile page after successful edit of profile with valid personal information|Vangel|Medium|Unresolved
AQSI-8|Not all required fields are marked with '*' in profile editing page|Vangel|High|Unresolved
AQSI-9|Registered user cannot change profile picture visability without selecting new image|Vangel|High|Unresolved
AQSI-10|Systems prevents registration of user with numbers/symbols in username|Teodora|High|Unresolved
AQSI-11|System shows same message to user attempting login with wrong credentials and with a disabled profile|Teodora|Low|Unresolved
AQSI-12|User with disabled profile is visible to other users|Teodora|High|Unresolved
AQSI-13|Admin user has no access to user private posts|Teodora|High|Unresolved
AQSI-14|Admin user posts feed contains all admin posts|Teodora|Low|Unresolved
AQSI-15|Text and picture of post are missing when user attempts to edit post|Vangel|High|Unresolved
AQSI-16|Logged in user browsing latest posts feed initially sees only their own posts|Teodora|Low|Unresolved
AQSI-17|No message for successfully created comment and the user is not navigated to comments section after posting|Vangel|High|Unresolved
AQSI-18|Updating profile names and profession is not mandatory after registration|Teodora|High|Unresolved
AQSI-19|Professions are selected from a dropdown in profile, there is no way to add a new one|Teodora|High|Unresolved
AQSI-20|Skills managed by rest-skill-controller aren't used by the system|Teodora|Low|Unresolved
AQSI-21|No message for successful edit of comment and the user is not navigated to comments section after editing|Vangel|High|Unresolved
AQSI-22|User is not navigated to comments section of the post after deleting a comment|Vangel|High|Unresolved
AQSI-23|Comment text is not loaded during editing|Vangel|High|Unresolved
AQSI-24|User can create empty post|Vangel|High|Unresolved
AQSI-25|User can create empty comment|Vangel|High|Unresolved
AQSI-26| GET request getUserById API returns code 404 response for  valid user ID|Teodora|Low|Unresolved
AQSI-27|GET showProfilePosts request for post with like returns post ?liked? field with value ?false?|Teodora|Low|Unresolved
AQSI-28|GET showComments request for liked comment returns comment ?liked? field with value ?false?|Teodora|Low|Unresolved
AQSI-29|System doesn't validate password complexity|Teodora|High|Unresolved
AQSI-30|System doesn't display which user is logged in|Teodora|High|Unresolved
AQSI-31|Admin user can disable their own profile|Teodora|Low|Unresolved
AQSI-32|Registration with username shorter than system requires results in error page|Teodora|High|Unresolved
AQSI-33|Registration with username longer than system requires results in error page|Teodora|High|Unresolved
AQSI-34|More than one user can register with the same email|Teodora|High|Unresolved
AQSI-35|System doesn't limit password character count |Teodora|Medium|Unresolved
AQSI-36|Missing image in HTML|Teodora|Low|Unresolved
AQSI-37|HTML/CSS checker shows errors and warnings|Teodora|Low|Unresolved
AQSI-38|Latest activity tab in profile page shows only recent posts (not comments)|Vangel|Medium|Unresolved
AQSI-39|Error page opens when user attempts to browse public posts after creating post|Vangel|Low|Unresolved
AQSI-40|Error page opens when user attempts to browses category after creating post|Vangel|Low|Unresolved
AQSI-41|User can create post by selecting text file instead of picture|Vangel|Low|Unresolved
AQSI-42|Admin user doesn't see private profile pictures|Teodora|Medium|Unresolved
AQSI-43|Navigation bar menu buttons are not consistent on different pages|Vangel|High|Unresolved
AQSI-44|There are two buttons on navigation bar that lead to the same base URL|Vangel|High|Unresolved
AQSI-45|Clicking on hamburger menu button does not open navigation drawer|Vangel|High|Unresolved
AQSI-46|Not all responses of API REST requests are present and in JSON format|Vangel|Low|Unresolved
AQSI-47|DevTools console errors|Vangel|Medium|Unresolved
AQSI-48|Main page contains carousel element with data for non-existent users|Vangel|High|Unresolved
AQSI-49|There are spelling/grammar/punctuation mistakes in the system|Teodora|High|Unresolved
AQSI-50|No email confirmation functionality|Teodora|High|Unresolved

## 4. Test Cases covered by automated API tests

Key|Summary|Priority
--|--|--
AQS-6|Anonymous user search by registered user valid first name|High
AQS-10|Anonymous user registers with valid credentials in all fields|High
AQS-18|Anonymous user searches all users by leaving search fields empty|High
AQS-27|Registered user edits profile with valid personal information in all fields|High
AQS-30|Registered user edits professional profile services and availability with valid values|High
AQS-34|Registered user sends connect request|High
AQS-35|Registered user approves connect request|High
AQS-38|Registered user creates public post with picture and short text|High
AQS-39|Registered user edits the text and changes the picture with valid text and picture of post|High
AQS-40|Registered user deletes own post|High
AQS-41|Registered user creates comment with valid text|High
AQS-42|Registered user edits comment with valid text|High
AQS-43|Registered user deletes comment|High
AQS-53|Registered user likes post|Medium
AQS-80|Registered user likes comment|Medium

## 5. Test Cases covered by automated UI tests

Key|Summary|Priority
--|--|--
AQS-5|Anonymous user search by registered user valid profession|High
AQS-6|Anonymous user search by registered user valid first name|High
AQS-19|Anonymous user registers with valid credentials in required fields|High
AQS-29|Registered user changes professional profile industry|High
AQS-37|Registered user creates private post without picture with short text|High
AQS-80|Registered user likes comment|Medium
AQS-81|Registered user logs in with valid credentials|High

## 6. Test execution reports
* [ Manual Testing Report](https://saturnus.atlassian.net/secure/Dashboard.jspa?selectPageId=10002) (To view Jira Dashboard login with the following credentials:)
    * email: alpha.qa.saturnus@gmail.com
    * password: w&qVW>%9x6^[Sa+T   
    <br>
* [Automation UI Testing Surefire Report](https://t.futekova.gitlab.io/-/alpha-qa-saturnus-final-project/-/jobs/1461979625/artifacts/public/Surefire%20HTML%20Test%20Report/surefire-report.html)
* [Automation UI Testing Jenkins Report](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/jobs/1463315125/artifacts/file/public/Jenkins/Jenkins-UI-tests-execution-report.png)

* [Automation API Testing Newman Report](https://t.futekova.gitlab.io/-/alpha-qa-saturnus-final-project/-/jobs/1461979625/artifacts/public/newman/WEare-2021-07-29-11-21-36-407-0.html)

## 7. Conclusion
Based on the test results the conclusion is that the tested application is not ready for release.