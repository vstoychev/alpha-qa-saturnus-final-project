# Alpha QA Saturnus Final Project

## 1. [<img height=15 src="https://camo.githubusercontent.com/d1d6dc9dd94f2e9b6896fd9a3fd3b0fc5c30c98b9222cea348e84a971c711949/68747470733a2f2f63646e2e7261776769742e636f6d2f6a6f7368756162616b65722f63726166742d656d6265642f66383438633137382f7265736f75726365732f69636f6e2e737667"> Test Plan](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/blob/master/01.%20Planning%20And%20Control/TestPlan.md)

## 2. [<img height=15 src="https://camo.githubusercontent.com/d1d6dc9dd94f2e9b6896fd9a3fd3b0fc5c30c98b9222cea348e84a971c711949/68747470733a2f2f63646e2e7261776769742e636f6d2f6a6f7368756162616b65722f63726166742d656d6265642f66383438633137382f7265736f75726365732f69636f6e2e737667"> Mind Map Classification tree](https://drive.google.com/file/d/1505Pfl97lkohvGqMZv5QnAwAKpgDhZrk/view?usp=sharing)
* Used for test case design guide

## 3. [<img height=15 src="https://camo.githubusercontent.com/d1d6dc9dd94f2e9b6896fd9a3fd3b0fc5c30c98b9222cea348e84a971c711949/68747470733a2f2f63646e2e7261776769742e636f6d2f6a6f7368756162616b65722f63726166742d656d6265642f66383438633137382f7265736f75726365732f69636f6e2e737667"> Trello Board](https://trello.com/b/nlOGUuKE/alpha-qa-saturnus-final-project-board)
* To view Trello Board login with the following credentials:
    - email: alpha.qa.saturnus@gmail.com
    - password: w&qVW>%9x6^[Sa+T

## 4. [<img height=15 src="https://camo.githubusercontent.com/d1d6dc9dd94f2e9b6896fd9a3fd3b0fc5c30c98b9222cea348e84a971c711949/68747470733a2f2f63646e2e7261776769742e636f6d2f6a6f7368756162616b65722f63726166742d656d6265642f66383438633137382f7265736f75726365732f69636f6e2e737667"> Exploratory Testing Artifacts](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/tree/master/02.%20Analysis%20And%20Design/Exploratory%20Tests)
* View HTML reports or add [Exploratory Testing Chrome Extension](https://chrome.google.com/webstore/detail/exploratory-testing-chrom/khigmghadjljgjpamimgjjmpmlbgmekj?hl=en) to your browser and import json files to view a better structured session report

## 5. [<img height=15 src="https://camo.githubusercontent.com/d1d6dc9dd94f2e9b6896fd9a3fd3b0fc5c30c98b9222cea348e84a971c711949/68747470733a2f2f63646e2e7261776769742e636f6d2f6a6f7368756162616b65722f63726166742d656d6265642f66383438633137382f7265736f75726365732f69636f6e2e737667"> Test Cases](https://saturnus.atlassian.net/jira/software/projects/AQS/issues/)
* To view the Jira Project login with the following credentials:
    - email: alpha.qa.saturnus@gmail.com
    - password: w&qVW>%9x6^[Sa+T

## 6. [<img height=15 src="https://camo.githubusercontent.com/d1d6dc9dd94f2e9b6896fd9a3fd3b0fc5c30c98b9222cea348e84a971c711949/68747470733a2f2f63646e2e7261776769742e636f6d2f6a6f7368756162616b65722f63726166742d656d6265642f66383438633137382f7265736f75726365732f69636f6e2e737667"> Bugs](https://saturnus.atlassian.net/jira/software/projects/AQSI/issues/)
* To view the Jira Project login with the following credentials:
    - email: alpha.qa.saturnus@gmail.com
    - password: w&qVW>%9x6^[Sa+T

## 7. [<img height=15 src="https://camo.githubusercontent.com/d1d6dc9dd94f2e9b6896fd9a3fd3b0fc5c30c98b9222cea348e84a971c711949/68747470733a2f2f63646e2e7261776769742e636f6d2f6a6f7368756162616b65722f63726166742d656d6265642f66383438633137382f7265736f75726365732f69636f6e2e737667"> Automated UI Tests](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/tree/master/03.%20Implementation%20And%20Execution/UI)

## 8. [<img height=15 src="https://camo.githubusercontent.com/d1d6dc9dd94f2e9b6896fd9a3fd3b0fc5c30c98b9222cea348e84a971c711949/68747470733a2f2f63646e2e7261776769742e636f6d2f6a6f7368756162616b65722f63726166742d656d6265642f66383438633137382f7265736f75726365732f69636f6e2e737667"> Automated API Tests](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/tree/master/03.%20Implementation%20And%20Execution/API)

## 9. [<img height=15 src="https://camo.githubusercontent.com/d1d6dc9dd94f2e9b6896fd9a3fd3b0fc5c30c98b9222cea348e84a971c711949/68747470733a2f2f63646e2e7261776769742e636f6d2f6a6f7368756162616b65722f63726166742d656d6265642f66383438633137382f7265736f75726365732f69636f6e2e737667"> Manual Testing Report](https://saturnus.atlassian.net/secure/Dashboard.jspa?selectPageId=10002)
* To view Jira Dashboard login with the following credentials:
    - email: alpha.qa.saturnus@gmail.com
    - password: w&qVW>%9x6^[Sa+T

## 10. [<img height=15 src="https://camo.githubusercontent.com/d1d6dc9dd94f2e9b6896fd9a3fd3b0fc5c30c98b9222cea348e84a971c711949/68747470733a2f2f63646e2e7261776769742e636f6d2f6a6f7368756162616b65722f63726166742d656d6265642f66383438633137382f7265736f75726365732f69636f6e2e737667"> Automation UI Testing Surefire Report](https://t.futekova.gitlab.io/-/alpha-qa-saturnus-final-project/-/jobs/1461979625/artifacts/public/Surefire%20HTML%20Test%20Report/surefire-report.html)

## 11. [<img height=15 src="https://camo.githubusercontent.com/d1d6dc9dd94f2e9b6896fd9a3fd3b0fc5c30c98b9222cea348e84a971c711949/68747470733a2f2f63646e2e7261776769742e636f6d2f6a6f7368756162616b65722f63726166742d656d6265642f66383438633137382f7265736f75726365732f69636f6e2e737667"> Automation UI Testing Jenkins Report](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/jobs/1463315125/artifacts/file/public/Jenkins/Jenkins-UI-tests-execution-report.png)

## 12. [<img height=15 src="https://camo.githubusercontent.com/d1d6dc9dd94f2e9b6896fd9a3fd3b0fc5c30c98b9222cea348e84a971c711949/68747470733a2f2f63646e2e7261776769742e636f6d2f6a6f7368756162616b65722f63726166742d656d6265642f66383438633137382f7265736f75726365732f69636f6e2e737667"> Automation API Testing Newman Report](https://t.futekova.gitlab.io/-/alpha-qa-saturnus-final-project/-/jobs/1461979625/artifacts/public/newman/WEare-2021-07-29-11-21-36-407-0.html)

## 13. [<img height=15 src="https://camo.githubusercontent.com/d1d6dc9dd94f2e9b6896fd9a3fd3b0fc5c30c98b9222cea348e84a971c711949/68747470733a2f2f63646e2e7261776769742e636f6d2f6a6f7368756162616b65722f63726166742d656d6265642f66383438633137382f7265736f75726365732f69636f6e2e737667"> Test Report](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/blob/master/04.%20Exit%20Criteria%20And%20Reporting/TestReport.md)
