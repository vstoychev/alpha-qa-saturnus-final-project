# Requirements for UI tests execution

## Alpha QA Saturnus Final Project

### 1. Prerequisites
* Windows OS
* Locally installed WeAre application testing environment (for local tests) - [Instructions Link](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/tree/master/Instructions/Local%20Testing%20Environment%20Setup)
* Google Chrome or Mozilla Firefox installed
* [JDK 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) installed on host
* An IDE installed to view code ([IntelliJ IDEA](https://www.jetbrains.com/idea/resources/) for example)
* [Maven ](https://maven.apache.org/install.html)installed and set in Environment variables
* Jenkins setup (optional)
    - [Docker image ](https://www.jenkins.io/doc/book/installing/docker/) installation
    - [Windows agent ](https://wiki.jenkins.io/display/JENKINS//Step+by+step+guide+to+set+up+master+and+agent+machines+on+Windows) installation

### 2. Execution Steps UI tests
* Open project with chosen IDE (optional)
* Running tests options:
    * Run tests from IDE 
    * Choose and download any batch file from section 3. and execute on Windows host 
    * Configure and run [Jenkins pipeline](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/blob/master/Jenkinsfile)

### 3.Details about the tests
* [clone-repository-and-run-all-tests-with-chrome-on-local-environment](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/blob/master/03.%20Implementation%20And%20Execution/UI/Batch%20files%20for%20UI%20tests/clone-repository-and-run-all-tests-with-chrome-on-local-environment.bat) - includes all tests from suites and runs them with **Chrome** browser on **local** environment 
* [clone-repository-and-run-all-tests-with-firefox-on-local-environment](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/blob/master/03.%20Implementation%20And%20Execution/UI/Batch%20files%20for%20UI%20tests/clone-repository-and-run-all-tests-with-firefox-on-local-environment.bat) - includes all tests from suites and runs them with **Firefox** browser on **local** environment
* [clone-repository-and-run-one-test-with-chrome-on-local-environment](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/blob/master/03.%20Implementation%20And%20Execution/UI/Batch%20files%20for%20UI%20tests/clone-repository-and-run-one-test-with-chrome-on-local-environment.bat) - includes createPrivatePost_When_OnlyValidTextIsProvided test from Post suite and runs it with **Chrome** browser on **local** environment
* [clone-repository-and-run-one-test-with-firefox-on-local-environment](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/blob/master/03.%20Implementation%20And%20Execution/UI/Batch%20files%20for%20UI%20tests/clone-repository-and-run-one-test-with-firefox-on-local-environment.bat) - includes createPrivatePost_When_OnlyValidTextIsProvided test from Post suite and runs it with **Firefox** browser on **local** environment
* [clone-repository-and-run-one-test-with-chrome-on-cloud-environment](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/blob/master/03.%20Implementation%20And%20Execution/UI/Batch%20files%20for%20UI%20tests/clone-repository-and-run-one-test-with-chrome-on-cloud-environment.bat) - includes loginUser_When_ValidCredentialsAreEntered test from Login suite and runs it with **Chrome** browser on **cloud** environment