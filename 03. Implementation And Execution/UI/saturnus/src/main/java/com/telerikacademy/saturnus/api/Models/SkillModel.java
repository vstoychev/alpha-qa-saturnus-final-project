package com.telerikacademy.saturnus.api.Models;

public class SkillModel {
    public CategoryModel category;
    public String skill;
    public int skillId;

    public SkillModel() {
        category = new CategoryModel();
    }
}
