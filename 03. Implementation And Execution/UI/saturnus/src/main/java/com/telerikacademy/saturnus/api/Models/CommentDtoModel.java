package com.telerikacademy.saturnus.api.Models;

public class CommentDtoModel {
    public String content;
    public int postId;
    public int userId;

    public String toJSONString() {
        return String.format("{" + System.lineSeparator() +
                "  \"content\": \"%s\"," + System.lineSeparator() +
                "  \"postId\": %d," + System.lineSeparator() +
                "  \"userId\": %d" + System.lineSeparator() +
                "}", content, postId, userId);
    }
}
