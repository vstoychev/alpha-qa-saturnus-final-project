package com.telerikacademy.saturnus.pages;

import com.telerikacademy.saturnus.Utils;

import java.util.Map;

public class RegistrationPage extends BasePage {

    public RegistrationPage() {
        super("weAre.registrationPage");
    }

    public void registerUserWithRandomCredentials() {
        actions.waitForElementVisibleUntilTimeout("weAre.registrationPage.usernameField", 5);
        Map<String, String> credentials = Utils.getRandomCredentials();

        registerUser(credentials.get("username"), credentials.get("email"), credentials.get("password"));
    }

    public void registerUser(String username, String email, String password) {
        Utils.LOG.info("Registering new user with username: '" + username + "', email: '" + email + "' and password: '" + password + "'");
        actions.waitForElementVisibleUntilTimeout("weAre.registrationPage.usernameField", 5);

        actions.typeValueInField(username, "weAre.registrationPage.usernameField");
        actions.typeValueInField(email, "weAre.registrationPage.emailField");

        actions.typeValueInField(password, "weAre.registrationPage.passwordField");
        actions.typeValueInField(password, "weAre.registrationPage.confirmPasswordField");

        actions.clickElement("weAre.registrationPage.registerButton");
    }
}
