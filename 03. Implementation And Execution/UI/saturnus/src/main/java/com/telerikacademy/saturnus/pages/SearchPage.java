package com.telerikacademy.saturnus.pages;

import com.telerikacademy.saturnus.sections.NavigationBarSection;

public class SearchPage extends BasePage {

    public NavigationBarSection navigationBarSection;

    public SearchPage() {
        super("weAre.searchPage");
        navigationBarSection = NavigationBarSection.getInstance();
    }
}
