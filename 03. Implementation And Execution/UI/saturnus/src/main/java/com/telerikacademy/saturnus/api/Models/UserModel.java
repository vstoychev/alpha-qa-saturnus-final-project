package com.telerikacademy.saturnus.api.Models;

import java.util.ArrayList;
import java.util.List;

public class UserModel {
    public boolean accountNonExpired;
    public boolean accountNonLocked;
    public List<String> authorities;
    public boolean credentialsNonExpired;
    public String email;
    public boolean enabled;
    public ExpertiseProfileModel expertiseProfile;
    public String password;
    public PersonalProfileModel personalProfile;
    public int userId;
    public String username;

    public UserModel() {
        authorities = new ArrayList<>();
        personalProfile = new PersonalProfileModel();
        expertiseProfile = new ExpertiseProfileModel();
    }
}
