package com.telerikacademy.saturnus.sections;

import com.telerikacademy.saturnus.UserActions;

public class NavigationBarSection {
    private static NavigationBarSection singleInstance;
    public UserActions actions = new UserActions();

    public static NavigationBarSection getInstance() {
        if (singleInstance == null) {
            singleInstance = new NavigationBarSection();
        }
        return singleInstance;
    }

    public void navigateToUserProfilePage() {
        actions.waitForElementVisibleUntilTimeout("weAre.navigationBarSection.personalProfileButton", 5);
        actions.clickElement("weAre.navigationBarSection.personalProfileButton");
    }

    public void navigateToRegistrationPage() {
        actions.waitForElementVisibleUntilTimeout("weAre.navigationBarSection.registerButton", 5);
        actions.clickElement("weAre.navigationBarSection.registerButton");
    }

    public void navigateToLoginPage() {
        actions.waitForElementVisibleUntilTimeout("weAre.navigationBarSection.loginButton", 5);
        actions.clickElement("weAre.navigationBarSection.loginButton");
    }

    public void navigateToNewPostPage() {
        actions.waitForElementVisibleUntilTimeout("weAre.navigationBarSection.newPostButton", 5);
        actions.clickElement("weAre.navigationBarSection.newPostButton");
    }

    public void logoutUser() {
        if (actions.isElementPresent("weAre.navigationBarSection.logoutButton")) {
            actions.clickElement("weAre.navigationBarSection.logoutButton");
        }
    }

    public void navigateToHomePage(){
        actions.clickElement("weAre.navigationBarSection.homeButton");
    }

    public void navigateToPostsPage() { actions.clickElement("weAre.navigationBarSection.postsButton");}
}
