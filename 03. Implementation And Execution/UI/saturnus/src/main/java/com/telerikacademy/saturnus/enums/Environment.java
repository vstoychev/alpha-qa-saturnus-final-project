package com.telerikacademy.saturnus.enums;

public enum Environment {
    LOCAL, CLOUD;

    @Override
    public String toString() {
        switch (this) {
            case CLOUD:
                return "Cloud";
            case LOCAL:
            default:
                return "Local";
        }
    }
}
