package com.telerikacademy.saturnus.api.Models;

import java.util.ArrayList;
import java.util.List;

public class UserDtoModel {
    public List<String> authorities;
    public CategoryModel category;
    public String confirmPassword;
    public String email;
    public String password;
    public String username;

    public UserDtoModel() {
        category = new CategoryModel();
        authorities = new ArrayList<>();
        authorities.add(0, "Some String");
    }

    public String toJSONString() {
        return String.format("{" + System.lineSeparator() +
                "  \"authorities\": [" + System.lineSeparator() +
                "    \"%s\"" + System.lineSeparator() +
                "  ]," + System.lineSeparator() +
                "%s" + System.lineSeparator() +
                "  \"confirmPassword\": \"%s\"," + System.lineSeparator() +
                "  \"email\": \"%s\"," + System.lineSeparator() +
                "  \"password\": \"%s\"," + System.lineSeparator() +
                "  \"username\": \"%s\"" + System.lineSeparator() +
                "}", authorities.get(0), category.toJSONString(), confirmPassword, email, password, username);
    }
}
