package com.telerikacademy.saturnus.pages;

import com.telerikacademy.saturnus.sections.NavigationBarSection;

public class HomePage extends BasePage {

    public NavigationBarSection navigationBarSection;

    public HomePage() {
        super("weAre.homePage");
        navigationBarSection = NavigationBarSection.getInstance();
    }

    public void searchByProfession(String profession) {
        actions.typeValueInField(profession, "weAre.homePage.professionSearchField");
        actions.clickElement("weAre.homePage.searchButton");
    }

    public void searchByUser(String user) {
        actions.typeValueInField(user, "weAre.homePage.userSearchField");
        actions.clickElement("weAre.homePage.searchButton");
    }

}
