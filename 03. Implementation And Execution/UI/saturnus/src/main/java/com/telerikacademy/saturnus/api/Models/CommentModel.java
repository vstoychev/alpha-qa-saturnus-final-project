package com.telerikacademy.saturnus.api.Models;

import java.util.ArrayList;
import java.util.List;

public class CommentModel {
    public int commentId;
    public String content;
    public String date;
    public boolean liked;
    public List<UserModel> likes;
    public PostModel post;
    public UserModel user;

    public CommentModel() {
        likes = new ArrayList<>();
        post = new PostModel();
        user = new UserModel();
    }
}
