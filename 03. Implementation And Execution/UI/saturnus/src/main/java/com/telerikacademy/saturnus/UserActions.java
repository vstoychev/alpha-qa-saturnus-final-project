package com.telerikacademy.saturnus;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserActions {

    public void refreshBrowser() {
        Utils.LOG.info("Refreshing browser");

        Utils.getWebDriver().navigate().refresh();
    }

    public void clickElement(String key, Object... arguments) {
        Utils.LOG.info("Clicking on element " + key);

        String locator = getLocatorValueByKey(key, arguments);

        WebElement element = Utils.getWebDriver().findElement(By.xpath(locator));
        element.click();
    }

    public void hoverElement(String key, Object... arguments) {
        Utils.LOG.info("Hovering over element with locator: '" + key + "'");

        String locator = getLocatorValueByKey(key, arguments);
        WebElement element = Utils.getWebDriver().findElement(By.xpath(locator));

        Actions action = new Actions(Utils.getWebDriver());
        action.moveToElement(element);
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        Utils.LOG.info("Typing " + value + " in " + field);

        String locator = getLocatorValueByKey(field, fieldArguments);
        WebElement element = Utils.getWebDriver().findElement(By.xpath(locator));

        element.sendKeys(value);
    }

    public void switchToIFrame(String iframe) {
        Utils.LOG.info("Switching to iFrame " + iframe);

        WebElement iFrame = Utils.getWebDriver().findElement(By.xpath(Utils.getUIMappingByKey(iframe)));

        Utils.getWebDriver().switchTo().frame(iFrame);
    }

    public String getElementAttributeValue(String attribute, String locatorKey, Object... locatorArguments){
        Utils.LOG.info(
                String.format("Getting attribute '%s' of element with locator key '%s'",attribute, locatorKey));

        String xpath = getLocatorValueByKey(locatorKey, locatorArguments);

        return  Utils.getWebDriver().findElement(By.xpath(xpath)).getAttribute(attribute);
    }

    public String getElementText(String locatorKey, Object... locatorArguments){
        Utils.LOG.info(
                String.format("Getting text of element with locator key '%s'", locatorKey));

        String xpath = getLocatorValueByKey(locatorKey, locatorArguments);

        return  Utils.getWebDriver().findElement(By.xpath(xpath)).getText();
    }

    public void waitForElementVisible(String locatorKey, Object... arguments) {
        Utils.LOG.info("Waiting for element with locator: '" + locatorKey + "' to be visible");

        int defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));

        waitForElementVisibleUntilTimeout(locatorKey, defaultTimeout, arguments);
    }

    public void waitForElementVisibleUntilTimeout(String locator, int seconds, Object... locatorArguments) {
        Utils.LOG.info("Waiting " + seconds + " seconds for element with locator: '" + locator + "' to be visible");

        WebDriverWait wait = new WebDriverWait(Utils.getWebDriver(), seconds);
        String xpath = getLocatorValueByKey(locator, locatorArguments);

        try {
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
        } catch (Exception exception) {
            Assert.fail("Element with locator: '" + xpath + "' was not found.");
        }
    }

    public void waitForElementPresent(String locator, Object... arguments) {
        Utils.LOG.info("Waiting for element with locator: '" + locator + "' to be present");

        Assert.assertTrue("Element with locator: '" + locator + "' isn't present", isElementPresent(locator, arguments));
    }

    public boolean isElementPresent(String locator, Object... arguments) {
        Utils.LOG.info("Determining if element with locator: '" + locator + "' is present");

        int timeOutSeconds = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
        WebDriverWait wait = new WebDriverWait(Utils.getWebDriver(), timeOutSeconds);

        String key = getLocatorValueByKey(locator, arguments);
        try{
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(key)));
            return true;
        } catch(org.openqa.selenium.TimeoutException e){
            return false;
        }
    }

    public boolean isElementVisible(String locator, Object... arguments) {
        Utils.LOG.info("Determining if element with locator: '" + locator + "' is visible");

        int timeOutSeconds = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
        WebDriverWait wait = new WebDriverWait(Utils.getWebDriver(), timeOutSeconds);

        String key = getLocatorValueByKey(locator, arguments);
        try{
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(key)));
            return true;
        } catch(org.openqa.selenium.TimeoutException e){
            return false;
        }
    }

    public void waitFor(long timeOutMilliseconds) {
        Utils.LOG.info("Waiting for " + timeOutMilliseconds + " milliseconds");

        try {
            Thread.sleep(timeOutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void assertElementPresent(String locator) {
        Utils.LOG.info("Asserting element with locator: '" + locator + "' is present");

        Assert.assertNotNull(Utils.getWebDriver().findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementAttribute(String locator, String attributeName, String attributeValue) {
        Utils.LOG.info("Asserting element with locator: '" + locator + "' has attribute '" + attributeName + "' with value: '" + attributeValue + "'");

        WebElement element = Utils.getWebDriver().findElement(By.xpath(Utils.getUIMappingByKey(locator)));

        Assert.assertEquals("Element with locator: '" + locator + "' attribute" + attributeName + "does not equal" + attributeValue,
                element.getAttribute(attributeName), attributeValue);
    }

    public void assertNavigatedUrl(String urlKey) {
        Utils.LOG.info("Asserting URL: '" + urlKey + "' is navigated");

        Assert.assertTrue("Expected Url: " + urlKey + " navigated Url: " + Utils.getWebDriver().getCurrentUrl(),
                Utils.getWebDriver().getCurrentUrl().contains(Utils.getConfigPropertyByKey(urlKey)));
    }

    public void pressKey(Keys key) {
        Utils.LOG.info("Pressing " + key + " key");

        Actions action = new Actions(Utils.getWebDriver());
        action.keyDown(key).keyUp(key);
    }

    private String getLocatorValueByKey(String locator, Object[] arguments) {
        return String.format(Utils.getUIMappingByKey(locator), arguments);
    }

}
