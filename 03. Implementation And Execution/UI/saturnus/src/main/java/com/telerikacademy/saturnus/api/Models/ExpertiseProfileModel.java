package com.telerikacademy.saturnus.api.Models;

import java.util.ArrayList;
import java.util.List;

public class ExpertiseProfileModel {
    public int availability;
    public CategoryModel category;
    public int id;
    public List<SkillModel> skills;

    public ExpertiseProfileModel() {
        category = new CategoryModel();
        skills = new ArrayList<>();
    }
}
