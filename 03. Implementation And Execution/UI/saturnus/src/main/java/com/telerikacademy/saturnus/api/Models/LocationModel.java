package com.telerikacademy.saturnus.api.Models;

public class LocationModel {
    public CityModel city;
    public int id;

    public LocationModel() {
        city = new CityModel();
    }

    public String toJSONString() {
        return String.format("  \"location\": {" + System.lineSeparator() +
                "%s" + System.lineSeparator() +
                "    \"id\": 0" + System.lineSeparator() +
                "  },", city.toJSONString());
    }
}
