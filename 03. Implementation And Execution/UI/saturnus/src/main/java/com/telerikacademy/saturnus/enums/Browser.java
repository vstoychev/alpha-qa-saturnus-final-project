package com.telerikacademy.saturnus.enums;

public enum Browser {
    CHROME, FIREFOX;

    @Override
    public String toString() {
        switch (this) {
            case CHROME:
                return "Chrome";
            case FIREFOX:
                return "Firefox";
            default:
                return "Unknown";
        }
    }
}
