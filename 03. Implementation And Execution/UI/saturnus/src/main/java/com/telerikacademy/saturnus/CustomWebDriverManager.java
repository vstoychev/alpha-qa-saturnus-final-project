package com.telerikacademy.saturnus;

import com.telerikacademy.saturnus.enums.Browser;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CustomWebDriverManager {
	public enum CustomWebDriverManagerEnum {
		INSTANCE;
		private WebDriver driver;
		private Browser browser = Browser.CHROME;

		public void setBrowser(Browser browser) {
			this.browser = browser;
		}

		public WebDriver getDriver() {
			if (driver == null){
				setupDriver();
			}
			return driver;
		}

		public void quitDriver() {
			Utils.LOG.info("Quitting WebDriver");
			if (driver != null) {
				driver.quit();
				driver = null;
			}
		}

		private WebDriver setupDriver(){
			Utils.LOG.info(String.format("Initializing WebDriver for %s browser", browser));

			switch (browser) {
				case FIREFOX:
					WebDriverManager.firefoxdriver().setup();
					WebDriver firefoxDriver = new FirefoxDriver();
					firefoxDriver.manage().window().maximize();
					driver = firefoxDriver;
					return firefoxDriver;
				case CHROME:
				default:
					WebDriverManager.chromedriver().setup();
					WebDriver chromeDriver = new ChromeDriver();
					chromeDriver.manage().window().maximize();
					driver = chromeDriver;
					return chromeDriver;
			}
		}
	}
}
