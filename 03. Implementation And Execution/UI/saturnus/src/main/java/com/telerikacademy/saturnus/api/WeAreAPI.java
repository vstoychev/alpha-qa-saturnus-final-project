package com.telerikacademy.saturnus.api;

import com.telerikacademy.saturnus.Utils;
import com.telerikacademy.saturnus.api.Models.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class WeAreAPI {

    private String apiUrl;
    private String apiHost;
    private String sessionIdCookie;

    public WeAreAPI() {
        apiUrl = Utils.getConfigPropertyByKey(Utils.getEnvironmentBaseUrlKey());
        apiHost = apiUrl.split("//")[1];
        sessionIdCookie = "";
    }

    public UserModel getUserFromConfigurationByNameKey(String nameKey) {
        Utils.LOG.info(String.format("Getting user from configuration by name key '%s'", nameKey));

        UserModel user = new UserModel();

        String fullUserKey = "weAre.users." + nameKey;
        user.username = Utils.getConfigPropertyByKey(fullUserKey + ".username");
        user.password = Utils.getConfigPropertyByKey(fullUserKey + ".password");
        user.userId = Integer.parseInt(Utils.getConfigPropertyByKey(fullUserKey + ".userId"));

        return user;
    }

    public void authenticateUser(UserModel user) {
        Utils.LOG.info("Authenticating user");

        String apiErrorMessage = Utils.getConfigPropertyByKey("weAre.apiWrongUsernameOrPasswordMessage");
        int expectedStatusCode = 302;
        String authenticateUrl =
                String.format("%s/authenticate?username=%s&password=%s", apiUrl, user.username, user.password);

        Utils.LOG.info(String.format("POST URL is '%s'", authenticateUrl));

        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .post(authenticateUrl);

        int responseStatusCode = response.getStatusCode();
        String responseLocationHeader = response.getHeader("Location");
        String responseBodyAsString = response.getBody().asString();
        sessionIdCookie = response.getCookie("JSESSIONID");

        Utils.LOG.info(
                String.format("Response status code is '%d' and body is '%s'", responseStatusCode, responseBodyAsString));

        Assert.assertEquals("Status code is not as expected", expectedStatusCode, responseStatusCode);

        Utils.LOG.info(
                String.format("The sessionIdCookie is '%s' and response Location header is '%s'",
                        sessionIdCookie, responseLocationHeader));

        Utils.LOG.info(
                String.format("Authenticating second step with GET redirected URL: '%s'", responseLocationHeader));
        expectedStatusCode = 200;

        response = RestAssured.given()
                .contentType(ContentType.JSON)
                .header("Host", apiHost)
                .header("Referer", authenticateUrl)
                .when()
                .get(responseLocationHeader);

        responseStatusCode = response.getStatusCode();
        responseBodyAsString = response.getBody().asString();
        Utils.LOG.info(String.format("Response status code is '%d'", responseStatusCode));

//        Next code line is commented, because the log gets filled with whole HTML page.
//        It is left for troubleshooting purposes (if needed).
//        Utils.LOG.info(String.format("Response body is '%s'", responseBodyAsString));

        Assert.assertEquals("Status code is not as expected", expectedStatusCode,responseStatusCode);

        Utils.LOG.info(
                String.format("Asserting that authentication is successful " +
                        "(the response does not include error message : '%s')", apiErrorMessage));
        Assert.assertFalse(
                String.format("The authentication is unsuccessful " +
                        "(the response includes error message : '%s')", apiErrorMessage),
                responseBodyAsString.contains(apiErrorMessage));
    }

    public UserModel registerNewUserWithRandomData() {
        Utils.LOG.info("Registering new user with random data");

        UserDtoModel userToRegister = new UserDtoModel();
        UserModel registeredUser = new UserModel();

        int expectedStatusCode = 200;
        Map<String, String> credentials = Utils.getRandomCredentials();

        userToRegister.username = credentials.get("username");
        userToRegister.password = credentials.get("password");
        userToRegister.confirmPassword = userToRegister.password;
        userToRegister.email = credentials.get("email");

        Utils.LOG.info(
                String.format("Generated username, password and email are '%s', '%s' and '%s'",
                        userToRegister.username, userToRegister.password, userToRegister.email));

        userToRegister.category.id = Integer.parseInt(Utils.getConfigPropertyByKey("weAre.categoryId"));
        userToRegister.category.name = Utils.getConfigPropertyByKey("weAre.categoryName");

        String registerUrl = String.format("%s/api/users/", apiUrl);

        Utils.LOG.info(String.format("POST URL is '%s'", registerUrl));
        Utils.LOG.info(String.format("Request body is '%s'", userToRegister.toJSONString()));

        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .header("Host", apiHost)
                .body(userToRegister.toJSONString())
                .when()
                .post(registerUrl);

        int responseStatusCode = response.getStatusCode();
        String responseBodyAsString = response.getBody().asString();
        Utils.LOG.info(String.format("Response status code is '%d' and body is '%s'", responseStatusCode, responseBodyAsString));

        Assert.assertEquals("Status code is not as expected", expectedStatusCode, responseStatusCode);

        String[] registerUserResponseAsArrayOfStrings = responseBodyAsString.split(" ");
        registeredUser.userId = Integer.parseInt(registerUserResponseAsArrayOfStrings[6]);

        Utils.LOG.info(String.format("The registered user ID is '%s'", registeredUser.userId));

        Assert.assertTrue("The user ID could not be extracted from register response",
                registeredUser.userId > 0);

        registeredUser.username = userToRegister.username;
        registeredUser.password = userToRegister.password;
        registeredUser.email = userToRegister.email;
        registeredUser.expertiseProfile.category.id = Integer.parseInt(Utils.getConfigPropertyByKey("weAre.categoryId"));
        registeredUser.expertiseProfile.category.name = Utils.getConfigPropertyByKey("weAre.categoryName");

        return registeredUser;
    }

    public PersonalProfileModel editUserPersonalProfile(UserModel user){
        Utils.LOG.info("Updating user profile with random data in required fields");

        PersonalProfileModel personalProfileToUpdate = new PersonalProfileModel();

        String editPersonalProfileUrl = String.format("%s/api/users/auth/%d/personal", apiUrl, user.userId);
        int expectedStatusCode = 200;

        Map<String, String> personalProfileData = Utils.generateRandomProfileDataForRequiredFields();

        personalProfileToUpdate.birthYear = Utils.getConfigPropertyByKey("weAre.birthday");
        personalProfileToUpdate.firstName = personalProfileData.get("firstName");
        personalProfileToUpdate.id = user.userId;
        personalProfileToUpdate.lastName = personalProfileData.get("lastName");

        personalProfileToUpdate.location.city.city = Utils.getConfigPropertyByKey("weAre.cityName");
        personalProfileToUpdate.location.city.id = Integer.parseInt(Utils.getConfigPropertyByKey("weAre.cityId"));

        personalProfileToUpdate.memberSince = personalProfileData.get("memberSince");
        personalProfileToUpdate.personalReview = "";
        personalProfileToUpdate.picture = "";
        personalProfileToUpdate.picturePrivacy = true;
        personalProfileToUpdate.sex = Utils.getConfigPropertyByKey("weAre.sex");

        Utils.LOG.info(String.format("POST URL is '%s'", editPersonalProfileUrl));
        Utils.LOG.info(String.format("Cookie is '%s'", sessionIdCookie));
        Utils.LOG.info(String.format("Request body is '%s'", personalProfileToUpdate.toJSONString()));

        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .header("Host", apiHost)
                .cookie("JSESSIONID", sessionIdCookie)
                .body(personalProfileToUpdate.toJSONString())
                .when()
                .post(editPersonalProfileUrl);

        int responseStatusCode = response.getStatusCode();
        String responseBodyAsString = response.getBody().asString();
        Utils.LOG.info(
                String.format("Response status code is '%d' and body is '%s'",
                        responseStatusCode, responseBodyAsString));

        Assert.assertEquals("Status code is not as expected", expectedStatusCode, responseStatusCode);

        PersonalProfileModel updatedPersonalProfile = response.getBody().as(PersonalProfileModel.class);

        Utils.LOG.info(String.format("The updated personal profile ID is '%s'", updatedPersonalProfile.id));

        Assert.assertTrue("The updated personal profile ID could not be extracted from update personal profile response",
                updatedPersonalProfile.id > 0);

        user.personalProfile = updatedPersonalProfile;

        return updatedPersonalProfile;
    }

    public PostModel createNewPostWithPictureAndRandomContent() {
        Utils.LOG.info("Creating new post with picture and random content");

        PostDtoModel postToCreate = new PostDtoModel();

        String createPostUrl = String.format("%s/api/post/auth/creator", apiUrl);
        int expectedStatusCode = 200;

        postToCreate.content = Utils.generateRandomPostContent();
        postToCreate.picture = Utils.getConfigPropertyByKey("weAre.samplePictureEncoded");
        postToCreate.isPublic = true;

        Utils.LOG.info(String.format("POST URL is '%s'", createPostUrl));
        Utils.LOG.info(String.format("Cookie is '%s'", sessionIdCookie));
        Utils.LOG.info(String.format("Request body is '%s'", postToCreate.toJSONString()));

        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .header("Host", apiHost)
                .cookie("JSESSIONID", sessionIdCookie)
                .body(postToCreate.toJSONString())
                .when()
                .post(createPostUrl);

        int responseStatusCode = response.getStatusCode();
        String responseBodyAsString = response.getBody().asString();
        Utils.LOG.info(
                String.format("Response status code is '%d' and body is '%s'",
                        responseStatusCode, responseBodyAsString));

        Assert.assertEquals("Status code is not as expected", expectedStatusCode, responseStatusCode);

        PostModel post = response.getBody().as(PostModel.class);

        Utils.LOG.info(String.format("The created post ID is '%s'", post.postId));

        Assert.assertTrue("The created post ID could not be extracted from create post response",
                post.postId > 0);

        //Field isPublic is used instead of public, that is why we have to set it
        post.isPublic = postToCreate.isPublic;

        return post;
    }

    public void deletePost(PostModel postToDelete) {
        Utils.LOG.info("Deleting post");
        String deletePostUrl = String.format("%s/api/post/auth/manager?postId=%d", apiUrl, postToDelete.postId);
        int expectedStatusCode = 200;

        Utils.LOG.info(String.format("DELETE URL is '%s'", deletePostUrl));
        Utils.LOG.info(String.format("Cookie is '%s'", sessionIdCookie));

        RestAssured.given()
                .contentType(ContentType.JSON)
                .header("Host", apiHost)
                .cookie("JSESSIONID", sessionIdCookie)
                .when()
                .delete(deletePostUrl)
                .then()
                .statusCode(expectedStatusCode);
    }

    public CommentModel createNewCommentWithRandomContent(UserModel user, PostModel post) {
        Utils.LOG.info("Creating new comment with random content");

        CommentDtoModel commentToCreate = new CommentDtoModel();

        String createCommentUrl = String.format("%s/api/comment/auth/creator", apiUrl);
        int expectedStatusCode = 200;

        commentToCreate.content = Utils.generateRandomCommentContent();
        commentToCreate.postId = post.postId;
        commentToCreate.userId = user.userId;

        Utils.LOG.info(String.format("POST URL is '%s'", createCommentUrl));
        Utils.LOG.info(String.format("Cookie is '%s'", sessionIdCookie));
        Utils.LOG.info(String.format("Request body is '%s'", commentToCreate.toJSONString()));

        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .header("Host", apiHost)
                .cookie("JSESSIONID", sessionIdCookie)
                .body(commentToCreate.toJSONString())
                .when()
                .post(createCommentUrl);

        int responseStatusCode = response.getStatusCode();
        String responseBodyAsString = response.getBody().asString();
        Utils.LOG.info(String.format("Response status code is '%d' and body is '%s'", responseStatusCode, responseBodyAsString));

        Assert.assertEquals("Status code is not as expected", expectedStatusCode, responseStatusCode);

        CommentModel comment = response.getBody().as(CommentModel.class);

        Utils.LOG.info(String.format("The created comment ID is '%s'", comment.commentId));

        Assert.assertTrue("The created comment ID could not be extracted from create comment response",
                comment.commentId > 0);

        return comment;
    }

    public void deleteComment(CommentModel commentToDelete) {
        Utils.LOG.info("Deleting comment");
        String deleteCommentUrl =
                String.format("%s/api/comment/auth/manager?commentId=%d", apiUrl, commentToDelete.commentId);

        Utils.LOG.info(String.format("DELETE URL is '%s'", deleteCommentUrl));
        Utils.LOG.info(String.format("Cookie is '%s'", sessionIdCookie));

        RestAssured.given()
                .contentType(ContentType.JSON)
                .header("Host", apiHost)
                .cookie("JSESSIONID", sessionIdCookie)
                .when()
                .delete(deleteCommentUrl)
                .then()
                .statusCode(200);
    }

    public List<PostModel> getAllPosts() {
        Utils.LOG.info("Getting all posts");
        String getAllPostsUrl = String.format("%s/api/post/", apiUrl);

        Utils.LOG.info(String.format("GET URL is '%s'", getAllPostsUrl));

        List<PostModel> posts =
                Arrays.asList(
                        RestAssured.given()
                                .contentType(ContentType.JSON)
                                .when()
                                .get(getAllPostsUrl)
                                .then()
                                .statusCode(200)
                                .extract().response()
                                .as(PostModel[].class));

        for (PostModel post : posts) {
            Utils.LOG.info(String.format("Returned post content is '%s'", post.content));
        }
        return posts;
    }
}