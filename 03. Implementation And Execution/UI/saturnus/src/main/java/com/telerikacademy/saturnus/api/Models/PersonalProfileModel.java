package com.telerikacademy.saturnus.api.Models;

public class PersonalProfileModel {
    public String birthYear;
    public String firstName;
    public int id;
    public String lastName;
    public LocationModel location;
    public String memberSince;
    public String personalReview;
    public String picture;
    public boolean picturePrivacy;
    public String sex;

    public PersonalProfileModel() {
        location = new LocationModel();
    }

    public String toJSONString() {
        return String.format("{" + System.lineSeparator() +
                "  \"birthYear\": \"%s\"," + System.lineSeparator() +
                "  \"firstName\": \"%s\"," + System.lineSeparator() +
                "  \"id\": %d," + System.lineSeparator() +
                "  \"lastName\": \"%s\"," + System.lineSeparator() +
                "%s" + System.lineSeparator() +
                "  \"memberSince\": \"%s\"," + System.lineSeparator() +
                "  \"personalReview\": \"%s\"," + System.lineSeparator() +
                "  \"picture\": \"%s\"," + System.lineSeparator() +
                "  \"picturePrivacy\": %b," + System.lineSeparator() +
                "  \"sex\": \"%s\"" + System.lineSeparator() +
                "}", birthYear, firstName, id, lastName, location.toJSONString(), memberSince, personalReview, picture, picturePrivacy, sex);
    }

}
