package com.telerikacademy.saturnus.api.Models;

import java.util.ArrayList;
import java.util.List;

public class PostModel {
    public CategoryModel category;
    public List<CommentModel> comments;
    public String content;
    public String date;
    public boolean liked;
    public List<UserModel> likes;
    public String picture;
    public int postId;
    public boolean isPublic;
    public int rank;
    public UserModel user;

    public PostModel() {
        category = new CategoryModel();
        comments = new ArrayList<>();
        likes = new ArrayList<>();
        user = new UserModel();
    }
}
