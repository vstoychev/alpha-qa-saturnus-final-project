package com.telerikacademy.saturnus.pages;

import com.telerikacademy.saturnus.Utils;

public class UserProfilePage extends BasePage {

    public UserProfilePage(String userIdAsString) {
        super( "weAre.userProfilePage", userIdAsString);
    }

    public void editUserProfile() {
        Utils.LOG.info("Editing user profile");

        actions.waitForElementVisibleUntilTimeout("weAre.userProfilePage.editProfileButton", 5);
        actions.clickElement("weAre.userProfilePage.editProfileButton");
    }

    public String getUserIndustry(){
        actions.waitForElementVisibleUntilTimeout("weAre.userProfilePage.userIndustry",5);
        return actions.getElementText("weAre.userProfilePage.userIndustry");
    }

}
