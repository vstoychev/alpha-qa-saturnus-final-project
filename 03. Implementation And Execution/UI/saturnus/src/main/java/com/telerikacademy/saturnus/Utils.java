package com.telerikacademy.saturnus;

import com.telerikacademy.saturnus.enums.Browser;
import com.telerikacademy.saturnus.enums.Environment;
import org.openqa.selenium.WebDriver;

import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Utils {

    private static Properties uiMappings = PropertiesManager.PropertiesManagerEnum.INSTANCE.getUiMappings();
    private static Properties configProperties = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties();
    public static final Logger LOG = LogManager.getRootLogger();
    private static Environment environment = Environment.LOCAL;

    public static void setupBrowserAndEnvironment() {
        String browserSystemProperty = System.getProperty("browser");
        if (browserSystemProperty != null) {
            CustomWebDriverManager.CustomWebDriverManagerEnum.INSTANCE.
            setBrowser(Browser.valueOf(browserSystemProperty.toUpperCase()));
        }

        String environmentSystemProperty = System.getProperty("environment");
        if (environmentSystemProperty != null) {
            setEnvironment(Environment.valueOf(environmentSystemProperty.toUpperCase()));
        }
    }

    public static WebDriver getWebDriver() {
        return CustomWebDriverManager.CustomWebDriverManagerEnum.INSTANCE.getDriver();
    }

    public static void tearDownWebDriver() {
        CustomWebDriverManager.CustomWebDriverManagerEnum.INSTANCE.quitDriver();
    }

    public static String getUIMappingByKey(String key) {
        String value = uiMappings.getProperty(key);
        return value != null ? value : key;
    }

    public static Properties getConfigProperties() {
        return PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties();
    }

    public static String getConfigPropertyByKey(String key,Object... keyArguments) {
        return String.format(configProperties.getProperty(key), keyArguments);
    }

    public static Map<String, String> getRandomCredentials() {
        Map<String, String> credentials = new HashMap<>();

        String username = "AQS" + generateRandomLowerCaseAlphabeticStringWithGivenLength(10);
        String email = generateRandomLowerCaseAlphabeticStringWithGivenLength(7) +
                "@" + generateRandomLowerCaseAlphabeticStringWithGivenLength(7) +
                ".com";
        String password = generateRandomLowerCaseAlphabeticStringWithGivenLength(6);

        credentials.put("username", username);
        credentials.put("email", email);
        credentials.put("password", password);

        return credentials;
    }

    public static Map<String, String> generateRandomProfileDataForRequiredFields() {
        Map<String, String> profileData = new HashMap<>();

        String firstName = generateRandomLowerCaseAlphabeticStringWithGivenLength(10);
        String lastName = generateRandomLowerCaseAlphabeticStringWithGivenLength(10);
        String memberSince = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").format(new Date());

        profileData.put("firstName", firstName);
        profileData.put("lastName", lastName);
        profileData.put("memberSince", memberSince);

        return profileData;
    }

    public static String generateRandomPostContent() {
        LOG.debug("Generating random post content");
        return "AQS New post: " + generateTimestamp() + " " +
                generateRandomLowerCaseAlphabeticStringWithGivenLength(5);

    }

    public static String generateRandomCommentContent() {
        LOG.debug("Generating random comment content");
        return "AQS new comment content : " + generateTimestamp() + " " +
                generateRandomLowerCaseAlphabeticStringWithGivenLength(5);

    }

    private static String generateRandomLowerCaseAlphabeticStringWithGivenLength(int stringLength) {
        LOG.debug("Generating random alphabetic string with " + stringLength + " lower case characters");

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(stringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static String generateTimestamp() {
        LOG.debug("Generating timestamp");

        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

    public static void setEnvironment(Environment environment) {
        LOG.debug(String.format("Setting Utils.environment to '%s'", environment));
        Utils.environment = environment;
    }

    public static void openBaseUrlInBrowser() {
        String baseUrlKey = getEnvironmentBaseUrlKey();
        getWebDriver().get(getConfigPropertyByKey(baseUrlKey));
    }

    public static String getEnvironmentBaseUrlKey() {
        switch (environment) {
            case CLOUD:
                return "base.cloud.url";
            case LOCAL:
            default:
                return "base.local.url";
        }
    }
}
