package com.telerikacademy.saturnus.pages;

import com.telerikacademy.saturnus.Utils;

public class ProfileEditorPage extends BasePage {

    public ProfileEditorPage(String userIdAsString) {
        super( "weAre.profileEditorPage", userIdAsString);
    }

    public String changeUserIndustry() {
        Utils.LOG.info("Changing user industry (professional category)");

        String newUserIndustry = selectNewIndustry();

        return newUserIndustry;
    }

    private String selectNewIndustry() {
        actions.waitForElementVisibleUntilTimeout("weAre.profileEditorPage.userIndustrySelectElement", 5);
        Utils.LOG.info(String.format("User industry (professional category) before selecting new one is '%s'",
                actions.getElementText("weAre.profileEditorPage.userIndustrySelectedOption")));

        actions.clickElement("weAre.profileEditorPage.userIndustrySelectElement");
        String newIndustry = actions.
                getElementText("weAre.profileEditorPage.userIndustryNotSelectedOptions");
        actions.clickElement("weAre.profileEditorPage.userIndustryNotSelectedOptions");
        actions.clickElement("weAre.profileEditorPage.userIndustryUpdateButton");

        Utils.LOG.info(
                String.format("Selected new industry (professional category) from dropdown is '%s'", newIndustry));

        return newIndustry;
    }

}
