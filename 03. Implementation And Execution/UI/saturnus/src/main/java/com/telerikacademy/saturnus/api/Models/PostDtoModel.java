package com.telerikacademy.saturnus.api.Models;

public class PostDtoModel {
    public String content;
    public String picture;
    public boolean isPublic;

    public String toJSONString() {
        return String.format("{" + System.lineSeparator() +
                "  \"content\": \"%s\"," + System.lineSeparator() +
                "  \"picture\": \"%s\"," + System.lineSeparator() +
                "  \"public\": %b" + System.lineSeparator() +
                "}", content, picture, isPublic);
    }
}
