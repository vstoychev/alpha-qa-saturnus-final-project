package com.telerikacademy.saturnus.pages;

import com.telerikacademy.saturnus.UserActions;
import com.telerikacademy.saturnus.Utils;
import org.junit.Assert;

public abstract class BasePage {
    protected String url;
    protected UserActions actions;

    public BasePage(String urlKey, Object... urlKeyArguments) {

        StringBuilder pageUrl = new StringBuilder();
        pageUrl.append(Utils.getConfigPropertyByKey(Utils.getEnvironmentBaseUrlKey(), urlKeyArguments));
        pageUrl.append(urlKey);

        this.url = pageUrl.toString();
        actions = new UserActions();
    }

    public String getUrl() {
        return url;
    }

    public void navigateToPage() {
        Utils.getWebDriver().get(url);
    }

    public void assertPageNavigated() {
        String currentUrl = Utils.getWebDriver().getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url, currentUrl.contains(url));
    }
}
