package com.telerikacademy.saturnus.api.Models;

public class CityModel {
    public String city;
    public CountryModel country;
    public int id;

    public CityModel() {
        country = new CountryModel();
    }

    public String toJSONString() {
        return String.format("    \"city\": {" + System.lineSeparator() +
                "      \"city\": \"%s\"," + System.lineSeparator() +
                "%s" + System.lineSeparator() +
                "      \"id\": %d" + System.lineSeparator() +
                "    },", city, country.toJSONString(), id);
    }
}
