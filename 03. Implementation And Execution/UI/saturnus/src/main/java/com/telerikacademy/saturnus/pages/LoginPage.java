package com.telerikacademy.saturnus.pages;

import com.telerikacademy.saturnus.Utils;

public class LoginPage extends BasePage {

    public LoginPage() {
        super("weAre.loginPage");
    }

    public void loginUser(String username, String password) {
        Utils.LOG.info(
                String.format("Logging in user with username: '%s' and password: '%s'",username,password));

        actions.waitForElementVisibleUntilTimeout("weAre.loginPage.usernameField", 5);

        actions.typeValueInField(username, "weAre.loginPage.usernameField");
        actions.typeValueInField(password, "weAre.loginPage.passwordField");
        actions.clickElement("weAre.loginPage.loginButton");
    }

}
