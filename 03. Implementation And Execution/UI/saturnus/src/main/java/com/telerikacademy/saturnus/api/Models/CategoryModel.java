package com.telerikacademy.saturnus.api.Models;

public class CategoryModel {
    public int id;
    public String name;

    public String toJSONString() {
        return String.format("  \"category\": {" + System.lineSeparator() +
                "    \"id\": %d," + System.lineSeparator() +
                "    \"name\": \"%s\"" + System.lineSeparator() +
                "  },", id, name);
    }
}
