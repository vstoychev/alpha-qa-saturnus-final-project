package com.telerikacademy.saturnus.pages;

import com.telerikacademy.saturnus.Utils;
import com.telerikacademy.saturnus.api.Models.PostModel;

public class PostsPage extends BasePage {

    public PostsPage() {
        super("weAre.newPostPage");
    }

    public void browseAllPublicPosts() {
        Utils.LOG.info("Browsing all public posts");
        actions.waitForElementVisibleUntilTimeout("weAre.postPage.browsePublicPostsButton", 5);
        actions.clickElement("weAre.postPage.browsePublicPostsButton");
    }

    public void explorePost(PostModel post) {
        Utils.LOG.info("Exploring post");
        actions.clickElement("weAre.postPage.explorePostsButton", post.postId);
    }

}
