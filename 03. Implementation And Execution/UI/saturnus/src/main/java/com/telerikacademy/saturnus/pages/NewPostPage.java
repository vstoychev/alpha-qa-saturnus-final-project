package com.telerikacademy.saturnus.pages;

import com.telerikacademy.saturnus.Utils;
import com.telerikacademy.saturnus.api.Models.CommentModel;
import com.telerikacademy.saturnus.api.Models.PostModel;
import com.telerikacademy.saturnus.sections.NavigationBarSection;

public class NewPostPage extends BasePage {

    public NavigationBarSection navigationBarSection;

    public NewPostPage() {
        super("weAre.newPostPage");
        navigationBarSection = NavigationBarSection.getInstance();
    }

    public PostModel createPostWithRandomText(boolean isPublic) {
        Utils.LOG.info(
                String.format("Creating post with random text with isPublic '%b'", isPublic));

        PostModel post = new PostModel();
        post.content = Utils.generateRandomPostContent();

        actions.waitForElementVisibleUntilTimeout("weAre.newPostPage.messageTextArea", 5);
        actions.typeValueInField(post.content, "weAre.newPostPage.messageTextArea");
        actions.clickElement("weAre.newPostPage.savePostButton");

        String hrefValue = actions.getElementAttributeValue(
                "href", "weAre.newPostPage.explorePostButton", post.content);

        post.postId = Integer.parseInt(hrefValue.split("/")[4]);
        Utils.LOG.info(String.format("The postId of the created post is '%d'",post.postId));

        return post;
    }

    public void showComments() {
        Utils.LOG.info("Showing post comments");
        actions.waitForElementVisibleUntilTimeout("weAre.newPostPage.showCommentsButton", 5);
        actions.clickElement("weAre.newPostPage.showCommentsButton");
    }

    public void likeComment(CommentModel comment) {
        Utils.LOG.info("Liking comment");
        actions.waitForElementVisible("weAre.newPostPage.likeCommentButton", comment.commentId);
        actions.clickElement("weAre.newPostPage.likeCommentButton", comment.commentId);
    }

    public int getCommentLikesCount(CommentModel comment) {
        Utils.LOG.info("Getting likes count");
        actions.waitForElementVisible("weAre.newPostPage.commentLikesCount", comment.commentId);
        int commentLikesCount = Integer.parseInt(
                actions.getElementText("weAre.newPostPage.commentLikesCount", comment.commentId).split(" ")[1]);
        Utils.LOG.info("Likes count is '" + commentLikesCount + "'");
        return commentLikesCount;
    }

}
