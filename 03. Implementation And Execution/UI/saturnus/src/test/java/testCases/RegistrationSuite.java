package testCases;

import com.telerikacademy.saturnus.Utils;
import com.telerikacademy.saturnus.pages.RegistrationPage;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Tag;

public class RegistrationSuite extends BaseTest {
    @Test
    @Tag("Registration")
    @Tag("AQS-19")
    public void registerUser_When_ValidCredentialsAreEnteredInRequiredFields() {
        Utils.LOG.info("Running " + name.getMethodName() + " test");
        home.navigationBarSection.navigateToRegistrationPage();

        RegistrationPage registration = new RegistrationPage();
        registration.registerUserWithRandomCredentials();

        Assert.assertTrue("Registration was unsuccessful",
                actions.isElementPresent("weAre.registrationPage.successfulRegistrationWelcomeMessage"));
    }
}
