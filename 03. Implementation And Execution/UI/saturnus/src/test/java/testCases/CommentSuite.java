package testCases;

import com.telerikacademy.saturnus.Utils;
import com.telerikacademy.saturnus.api.Models.UserModel;
import com.telerikacademy.saturnus.pages.LoginPage;
import com.telerikacademy.saturnus.pages.NewPostPage;
import com.telerikacademy.saturnus.pages.PostsPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Tag;

public class CommentSuite extends BaseTest{
    private UserModel secondUser;

    @Before
    public void initializeTestData() {
        registeredUser = weAreAPI.registerNewUserWithRandomData();
        weAreAPI.authenticateUser(registeredUser);
        createdPost = weAreAPI.createNewPostWithPictureAndRandomContent();
        createdComment = weAreAPI.createNewCommentWithRandomContent(registeredUser, createdPost);
        secondUser = weAreAPI.registerNewUserWithRandomData();
    }

    @Test
    @Tag("Comment")
    @Tag("AQS-80")
    public void likeComment_When_ThereIsAPostWithACommentInTheSystem(){
        Utils.LOG.info("Running " + name.getMethodName() + " test");
        home.navigationBarSection.navigateToLoginPage();

        LoginPage loginPage = new LoginPage();
        loginPage.loginUser(secondUser.username, secondUser.password);

        home.navigationBarSection.navigateToPostsPage();
        PostsPage postsPage = new PostsPage();
        postsPage.browseAllPublicPosts();
        postsPage.explorePost(createdPost);

        NewPostPage post = new NewPostPage();
        post.showComments();
        int commentLikesCountBeforeLike = post.getCommentLikesCount(createdComment);
        post.likeComment(createdComment);

        Assert.assertTrue("Dislike button isn't visible",
                actions.isElementVisible("weAre.newPostPage.dislikeCommentButton", createdComment.commentId));
        Assert.assertEquals("Likes count hasn't increased after like",
                post.getCommentLikesCount(createdComment), commentLikesCountBeforeLike + 1);
    }

    @After
    public void clearAfterTest(){
        NewPostPage post = new NewPostPage();
        post.navigationBarSection.logoutUser();

        weAreAPI.deleteComment(createdComment);
        weAreAPI.deletePost(createdPost);
    }

}
