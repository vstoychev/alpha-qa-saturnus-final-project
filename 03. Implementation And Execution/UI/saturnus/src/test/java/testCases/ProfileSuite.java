package testCases;

import com.telerikacademy.saturnus.Utils;
import com.telerikacademy.saturnus.pages.LoginPage;
import com.telerikacademy.saturnus.pages.ProfileEditorPage;
import com.telerikacademy.saturnus.pages.UserProfilePage;
import org.junit.*;
import org.junit.jupiter.api.Tag;

public class ProfileSuite extends BaseTest {

    @Before
    public void initializeTestData() {
        registeredUser = weAreAPI.registerNewUserWithRandomData();
    }

    @Test
    @Tag("Profile")
    @Tag("AQS-29")
    public void changeIndustry_When_EditingProfessionalProfile() {
        Utils.LOG.info("Running " + name.getMethodName() + " test");
        home.navigationBarSection.navigateToLoginPage();

        LoginPage loginPage = new LoginPage();
        loginPage.loginUser(registeredUser.username, registeredUser.password);

        home.navigationBarSection.navigateToUserProfilePage();

        UserProfilePage userProfilePage =
                new UserProfilePage(String.valueOf(registeredUser.userId));
        userProfilePage.editUserProfile();

        ProfileEditorPage profileEditorPage =
                new ProfileEditorPage(String.valueOf(registeredUser.userId));
        String newIndustry = profileEditorPage.changeUserIndustry();

        Assert.assertEquals("The user new industry (professional category) is not as expected",
                newIndustry,userProfilePage.getUserIndustry());
    }

    @After
    public void clearAfterTest() {
        home.navigationBarSection.logoutUser();
    }

}
