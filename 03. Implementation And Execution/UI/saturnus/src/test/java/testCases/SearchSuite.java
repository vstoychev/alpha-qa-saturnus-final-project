package testCases;

import com.telerikacademy.saturnus.Utils;
import com.telerikacademy.saturnus.pages.SearchPage;
import org.junit.*;
import org.junit.jupiter.api.Tag;

public class SearchSuite extends BaseTest {
    @Before
    public void initializeTestData() {
        registeredUser = weAreAPI.registerNewUserWithRandomData();
        weAreAPI.authenticateUser(registeredUser);
        personalProfile = weAreAPI.editUserPersonalProfile(registeredUser);
    }

    @Test
    @Tag("Search")
    @Tag("AQS-5")
    public void displayUser_When_ValidProfessionIsSearched() {
        Utils.LOG.info("Running " + name.getMethodName() + " test");

        home.searchByProfession(registeredUser.expertiseProfile.category.name);

        Assert.assertTrue("Search by profession was unsuccessful",
                actions.isElementPresent("weAre.homePage.searchResultByProfession", personalProfile.firstName));
    }

    @Test
    @Tag("Search")
    @Tag("AQS-6")
    public void displayUser_When_ValidFirstNameIsSearched() {
        Utils.LOG.info("Running " + name.getMethodName() + " test");

        home.searchByUser(registeredUser.personalProfile.firstName);

        Assert.assertTrue("Search by user first name was unsuccessful",
                actions.isElementPresent("weAre.homePage.searchResultByUser", personalProfile.firstName));
    }

    @After
    public void clearAfterTest(){
        SearchPage searchPagePage = new SearchPage();
        searchPagePage.navigationBarSection.navigateToHomePage();
    }
}
