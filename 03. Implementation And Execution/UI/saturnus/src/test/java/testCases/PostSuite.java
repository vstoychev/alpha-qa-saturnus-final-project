package testCases;

import com.telerikacademy.saturnus.Utils;
import com.telerikacademy.saturnus.pages.LoginPage;
import com.telerikacademy.saturnus.pages.NewPostPage;
import org.junit.*;
import org.junit.jupiter.api.Tag;

public class PostSuite extends BaseTest {
    @Before
    public void initializeTestData() {
        registeredUser = weAreAPI.registerNewUserWithRandomData();
        weAreAPI.authenticateUser(registeredUser);
    }

    @Test
    @Tag("Post")
    @Tag("AQS-37")
    public void createPrivatePost_When_OnlyValidTextIsProvided() {
        Utils.LOG.info("Running " + name.getMethodName() + " test");
        home.navigationBarSection.navigateToLoginPage();

        LoginPage loginPage = new LoginPage();
        loginPage.loginUser(registeredUser.username, registeredUser.password);

        home.navigationBarSection.navigateToNewPostPage();

        NewPostPage newPostPage = new NewPostPage();
        createdPost = newPostPage.createPostWithRandomText(false);

        Assert.assertTrue(String.format("Post with text/topic '%s' is not found on new post page", createdPost.content),
                actions.isElementVisible("weAre.newPostPage.postTopic", createdPost.content));
    }

    @After
    public void clearAfterTest() {
        home.navigationBarSection.logoutUser();
        if (createdPost.postId > 0) {
            weAreAPI.deletePost(createdPost);
        }
    }

}
