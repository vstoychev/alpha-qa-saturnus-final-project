package testCases;

import com.telerikacademy.saturnus.UserActions;
import com.telerikacademy.saturnus.Utils;
import com.telerikacademy.saturnus.api.Models.CommentModel;
import com.telerikacademy.saturnus.api.Models.PersonalProfileModel;
import com.telerikacademy.saturnus.api.Models.PostModel;
import com.telerikacademy.saturnus.api.Models.UserModel;
import com.telerikacademy.saturnus.api.WeAreAPI;
import com.telerikacademy.saturnus.pages.HomePage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestName;

public abstract class BaseTest {
    protected UserActions actions = new UserActions();
    protected WeAreAPI weAreAPI = new WeAreAPI();
    protected UserModel registeredUser;
    protected PersonalProfileModel personalProfile;
    protected PostModel createdPost = new PostModel();
    protected CommentModel createdComment = new CommentModel();
    protected HomePage home = new HomePage();

    @Rule
    public TestName name = new TestName();

    @BeforeClass
    public static void setUp() {
        Utils.setupBrowserAndEnvironment();
        Utils.openBaseUrlInBrowser();
    }

    @AfterClass
    public static void tearDown() {
        Utils.tearDownWebDriver();
    }
}
