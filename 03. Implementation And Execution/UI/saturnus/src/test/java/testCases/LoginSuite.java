package testCases;

import com.telerikacademy.saturnus.Utils;
import com.telerikacademy.saturnus.pages.LoginPage;
import org.junit.*;
import org.junit.jupiter.api.Tag;

public class LoginSuite extends BaseTest {
    @Before
    public void initializeTestData() {
        registeredUser = weAreAPI.registerNewUserWithRandomData();
    }

    @Test
    @Tag("Login")
    @Tag("AQS-81")
    public void loginUser_When_ValidCredentialsAreEntered() {
        Utils.LOG.info("Running " + name.getMethodName() + " test");
        home.navigationBarSection.navigateToLoginPage();

        LoginPage loginPage = new LoginPage();
        loginPage.loginUser(registeredUser.username, registeredUser.password);

        Assert.assertFalse("Login is unsuccessful (Wrong username or password error message is present)",
                actions.isElementPresent("weAre.loginPage.wrongUsernameOrPasswordErrorMessage"));
        actions.assertNavigatedUrl("weAre.homePage");
    }

    @After
    public void clearAfterTest() {
        home.navigationBarSection.logoutUser();
    }

}
