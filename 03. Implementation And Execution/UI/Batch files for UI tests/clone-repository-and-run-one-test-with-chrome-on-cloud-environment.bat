@echo off
echo Current JAVA_HOME is:
echo %JAVA_HOME%

for /f "delims=" %%A in ('echo %JAVA_HOME%') do set "CURRENT_JAVA_HOME=%%A"

if NOT "%CURRENT_JAVA_HOME%"=="%CURRENT_JAVA_HOME:jdk-11=%" (
    echo Current JAVA_HOME is to Java 11
) else (
    echo Current JAVA_HOME is not to Java 11
	echo Setting JAVA_HOME to the local JAVA 11
	for /f "delims=" %%B in ('dir /b "C:\Program Files\Java\jdk-11*"') do set "JAVA_HOME=C:\Program Files\Java\%%B"
)

rem pause

if exist temp\ (
	echo Temp directory exists, removing it
	rmdir /s /q temp  
) else (
 	echo Temp directory does not exist 
)

echo Creating temp direcory
mkdir temp
echo Change to the temp directory
cd temp

echo Clone the project
git clone https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project.git

echo Change to Saturnus project direcory
cd "alpha-qa-saturnus-final-project\03. Implementation And Execution\UI\saturnus"

echo Run loginUser_When_ValidCredentialsAreEntered test from LoginSuite on cloud environment with Chrome
call mvn clean surefire-report:report -Dtest=testCases.LoginSuite#loginUser_When_ValidCredentialsAreEntered -Denvironment=Cloud

echo Returning to the directory with the batch file
cd ..\..\..\..\..

echo Copying the HTML report and formating
mkdir temp\report
Xcopy /E /I "temp\alpha-qa-saturnus-final-project\public\Surefire HTML Test Report" temp\report\
del temp\report\surefire-report.html
copy "temp\alpha-qa-saturnus-final-project\03. Implementation And Execution\UI\saturnus\target\site\surefire-report.html" temp\report

echo The surefire-report.html can be found in "temp\report\" directory

if NOT "%JAVA_HOME%"=="%CURRENT_JAVA_HOME%" (
	echo Returning the JAVA_HOME before the change
	set JAVA_HOME=%CURRENT_JAVA_HOME%
)