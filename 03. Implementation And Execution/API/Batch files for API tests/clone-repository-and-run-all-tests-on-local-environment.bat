@echo off

if exist temp\ (
	echo Temp directory exists, removing it
	rmdir /s /q temp  
) else (
 	echo Temp directory does not exist 
)

echo Creating temp direcory
mkdir temp
echo Change to the temp directory
cd temp

echo Clone the project
git clone https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project.git

echo Change to Saturnus project API directory
cd "alpha-qa-saturnus-final-project\03. Implementation And Execution\API\saturnus"

echo Starting WEare REST APIs tests on local environment
newman run WEare.postman_collection.json -e WEare_local.postman_environment.json -r htmlextra
echo Finished WEare REST APIs tests. Report can be found in newman subdirectory.