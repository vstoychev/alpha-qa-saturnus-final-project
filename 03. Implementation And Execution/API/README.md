# Requirements for API tests execution

## Alpha QA Saturnus Final Project

### 1. Prerequisites
* Windows OS
* Locally installed WeAre application testing environment (for local tests) - [Instructions Link](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/tree/master/Instructions/Local%20Testing%20Environment%20Setup)
* Install [Postman](https://learning.postman.com/docs/getting-started/installation-and-updates/) (for running tests first option)
* Download and install [NodeJS](https://nodejs.org/en/) - (for running tests second option)
* Install Newman: CMD npm install -g newman - (for running tests second option) 
* Install HTMLExtra reporter: CMD npm install -g htmlextra - (for running tests second option)

### 2. Execution Steps API tests
* Open Postman and import collection and chosen environment (optional)
* Running tests options:
    * Run tests from Postman
    * Choose and download any batch file from section 3. and execute on Windows host

### 3.Details about the tests
* [clone-repository-and-run-all-tests-on-local-environment](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/blob/master/03.%20Implementation%20And%20Execution/API/Batch%20files%20for%20API%20tests/clone-repository-and-run-all-tests-on-local-environment.bat) - includes all API tests and runs them on **local** environment  
* [clone-repository-and-run-all-tests-on-cloud-environment](https://gitlab.com/t.futekova/alpha-qa-saturnus-final-project/-/blob/master/03.%20Implementation%20And%20Execution/API/Batch%20files%20for%20API%20tests/clone-repository-and-run-all-tests-on-cloud-environment.bat) - includes all API tests and runs them on **cloud** environment