# Test Plan
The purpose of this document is to outline the testing tools and techniques used in order to determine whether the product is ready for release.
## Contents
1. Introduction
2. Testing types  
    2.1 Exploratory Testing  
    2.2 Functional Manual Testing  
    2.3 Functional Automated Testing  
    2.4 REST API Testing
3. Scope of testing  
    3.1 Functionalities to be tested  
    3.2 Functionalities not to be tested  
4. Entry Criteria
5. Exit Criteria
6. Schedule
7. Tools
## 1. Introduction
The product that will be tested is a professional social network web application. It is used by users to connect with people, find a professional that can help and offer help in return.
## 2. Testing types
Overall testing approach is to have different types of testing to assure needed quality and to find problems as early as possible.
Participants in the tests are Saturnus team members from Alpha 28 QA cohort.
### 2.1 Exploratory Testing
* Definition  
    Exploratory testing for the whole system to cover main functionalities and happy paths will be executed.
* Methodology  
    Several happy paths will be selected. Every team member will conduct test independently covering those paths and the system's main functionalities. The results will be used as a base for the test cases definition.
    
### 2.2 Functional Manual Testing
* Definition  
    These tests will evaluate the system’s compliance with the specified functional requirements. The target is all requirements to be covered by tests.
* Methodology  
    The tests will be executed manually on a local test environment. Test cases must include - title, description, preconditions, steps to reproduce, expected result and priority. Bugs must include - title, description, preconditions, steps to reproduce, expected result, actual result, severity, priority, environment, used web browser and OS.

### 2.3 Functional Automated Testing
* Definition  
    Automated suites that cover some happy paths will be created.
* Methodology  
    The tests will have scripts for execution and integration with CI server so that different sets of tests can be run periodically.
    
### 2.4 REST API Testing
* Definition  
    Automated API test suites that cover all the methods identified for testing will be created.
* Methodology  
    The tests will have scripts for execution so that different sets of tests can be run periodically.

## 3. Scope of testing
### 3.1 Functionalities to be tested
* Registration
* Login
* Profile administration
* Friend requests
* Create post
* Edit post
* Delete post
* Like post
* Dislike post
* Comment under post
* Like comment
* Dislike comment
* Edit comment
* Delete comment
* Search
* Administration part
* API tags rest-comment-controller, rest-connection-controller, rest-post-controller, rest-skill-controller, rest-user-controller
### 3.2 Functionalities not to be tested
* Email notifications
* All other functionalities that are not listed in "Functionalities to be tested" section
## 4. Entry Criteria
* Test tools are selected and set up
* Test and bug workflows are defined, communicated, approved and implemented
* Local test environment is set up  
* Reporting tools are selected and set up
## 5. Exit Criteria 
* All test cases are executed​ and respective bugs are logged
* Deadline (30.07.2021) is reached
## 6. Schedule

|Activity                                                       | Start Date |  Due Date  |
|---------------------------------------------------------------|------------|------------|
|Test tools and servers/dockers set up                          | 05.07.2021 | 07.07.2021 |
|Test environment set up                                        | 05.07.2021 | 07.07.2021 |
|Ensure entry criteria is met                                   | 16.07.2021 | 16.07.2021 |
|Exploratory Testing                                            | 07.07.2021 | 08.07.2021 |
|Manual test cases design                                       | 09.07.2021 | 16.07.2021 |
|Test data preparation                                          | 14.07.2021 | 29.07.2021 |
|Manual tests execution                                         | 17.07.2021 | 29.07.2021 |
|Automated tests implementation                                 | 17.07.2021 | 29.07.2021 |
|REST API tests implementation                                  | 17.07.2021 | 22.07.2021 |
|CI server for automated tests configuration and implementation | 26.07.2021 | 29.07.2021 |
|Exit criteria and reporting activities                         | 27.07.2021 | 29.07.2021 |
|Closure activities                                             | 30.07.2021 | 30.07.2021 |

## 7. Tools
* Exploratory Testing Chrome Extension - used for exploratory testing and reporting
* Jira Cloud - used for test case management and bug tracking
* Jira Cloud Dashboard - used for manual test reports
* Selenium WebDriver - used for automated testing
* Rest Assured - used for test data generation and clean up
* Maven Surefire plugin - used for automated UI test reporting
* Jenkins - used for continuous integration and reporting
* Postman - used for REST API testing
* Newman - used for REST API reporting